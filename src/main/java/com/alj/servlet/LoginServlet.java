package com.alj.servlet;

import java.io.IOException;

import com.alj.service.EmployeeService;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@WebServlet("/loginPage")
public class LoginServlet extends HttpServlet {
	
	public LoginServlet() {
		super();
		System.out.println("inside constructor");
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside get method");
		
		EmployeeService service= new EmployeeService();
		
		String userid=req.getParameter("userid");
		String password= req.getParameter("password");
		
		if(userid.equals("admin")&& password.equals("password")) {
			RequestDispatcher rd = req.getRequestDispatcher("welcome.jsp");
			//req.setAttribute("id", userid);
			req.setAttribute("list", service.getEmployees());
			rd.forward(req, resp);
			System.out.println("successfully logged in");
		}
		else {
			//RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
			resp.sendRedirect("error.jsp");
			System.out.println("wrong credntials");
		}
		
		
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside post method");
		doGet(req,resp);
		

	}

}
