package com.alj.servlet;

import java.io.IOException;

import com.alj.beans.Employee;
import com.alj.service.EmployeeService;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/addEmployee")
public class EmployeeServlet extends HttpServlet {

	private EmployeeService service;

	public EmployeeServlet() {
		System.out.println("inside employee constructor");
		service = new EmployeeService();

	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside employee servlet class");
		RequestDispatcher rd = req.getRequestDispatcher("EmployeeForm.jsp");
		String employeeId = req.getParameter("empid");
		String empName = req.getParameter("empname");
		Employee emp = new Employee();
		emp.setEmployeeId(Integer.parseInt(employeeId));
		emp.setName(empName);
		try {
			req.setAttribute("message", service.addEmployee(emp));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			req.setAttribute("message", e.getMessage());

		}
		rd.forward(req, resp);

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside employee servlet class");
		doGet(req, resp);

	}
}
