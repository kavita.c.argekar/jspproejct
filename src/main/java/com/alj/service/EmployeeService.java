package com.alj.service;

import java.util.List;

import com.alj.beans.Employee;
import com.alj.repository.EmployeeRepository;

public class EmployeeService {

	private EmployeeRepository repo;

	public EmployeeService() {
		repo = new EmployeeRepository();
	}

	public List<Employee> getEmployees() {

		return repo.getEmployees();

	}
	
	public String addEmployee(Employee e) throws Exception  {
		return repo.addEmployee(e);
	}

}
