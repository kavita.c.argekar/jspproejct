package com.alj.repository;

import java.sql.*;

public class DBConnection {
	
	private  static Connection con=null;

	public static Connection getConnection() {
		
		try {
		    Class.forName("org.postgresql.Driver");
			con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/alj", "postgres", "postgres");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}

}
