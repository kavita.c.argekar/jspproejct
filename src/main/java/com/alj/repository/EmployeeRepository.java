package com.alj.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.alj.beans.Employee;

public class EmployeeRepository {

	private Connection con;

	public EmployeeRepository() {
		System.out.println("inside repo");
		con = DBConnection.getConnection();
	}
	
	
	public String addEmployee(Employee e) throws Exception  {
		String sql= "insert into employee1 values(?,?)";
		try {
			PreparedStatement pstmt= con.prepareStatement(sql);
			pstmt.setInt(1, e.getEmployeeId());
			pstmt.setString(2, e.getName());
			int i=pstmt.executeUpdate();
			if(i==1)
				return "employee added successfully";
			else
				return "not added";
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new Exception("ID already exists, please enter another employee id");
		}
	}

	public List<Employee> getEmployees() {
		String sql = "select * from employee1";
		List<Employee> list = new ArrayList<Employee>();

		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Employee e = new Employee();
				e.setEmployeeId(rs.getInt(1));
				e.setName(rs.getString(2));
				list.add(e);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;

	}

}
